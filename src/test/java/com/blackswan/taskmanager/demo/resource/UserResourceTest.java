package com.blackswan.taskmanager.demo.resource;

import com.blackswan.taskmanager.demo.domain.service.dto.UserDto;
import com.blackswan.taskmanager.demo.web.resource.UserResource;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class UserResourceTest {


    @Test
    public void test_mappingFromDto() {
        //given
        final var resource = UserResource.builder()
                .id(1L)
                .userName("username")
                .firstName("firstname")
                .lastName("lastname")
                .build();

        final var dto = UserDto.builder()
                .id(1L)
                .userName("username")
                .firstName("firstname")
                .lastName("lastname")
                .build();

        //when
        final var result = UserResource.fromDto(dto);

        //then
        assertNotNull(result);
        assertEquals(resource, result);
    }

    @Test
    public void test_mappingToDto() {
        //given
        final var resource = UserResource.builder()
                .id(1L)
                .userName("username")
                .firstName("firstname")
                .lastName("lastname")
                .build();

        final var dto = UserDto.builder()
                .id(1L)
                .userName("username")
                .firstName("firstname")
                .lastName("lastname")
                .build();

        //when
        final var result = UserResource.toUserDto(resource);

        //then
        assertNotNull(result);
        assertEquals(dto, result);
    }

}
