package com.blackswan.taskmanager.demo.service;

import com.blackswan.taskmanager.demo.dao.models.Task;
import com.blackswan.taskmanager.demo.dao.models.User;
import com.blackswan.taskmanager.demo.dao.repository.TaskRepository;
import com.blackswan.taskmanager.demo.dao.specification.FindDoneTasksSpecification;
import com.blackswan.taskmanager.demo.domain.service.TaskService;
import com.blackswan.taskmanager.demo.domain.service.dto.TaskDto;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class TaskServiceTest {

    @InjectMocks
    private TaskService taskService;

    @Mock
    private TaskRepository taskRepository;

    @Test
    public void test_save() {
        //given
        final var timeStamp = Timestamp.valueOf(LocalDateTime.now());

        final var user = new User()
                .setUserName("userName")
                .setFirstName("firstName")
                .setLastName("lastName")
                .setId(1L);

        final var taskDto = TaskDto.builder()
                .id(1L)
                .dateTime(timeStamp)
                .description("description")
                .name("task")
                .build();

        final var task = new Task()
                .setId(1L)
                .setDateTime(timeStamp)
                .setDescription("description")
                .setName("task")
                .setUser(user);

        when(taskRepository.save(eq(task))).thenReturn(task);
        //when
        final var result = taskService.save(taskDto, user);

        //then
        assertNotNull(result);
        assertEquals(task, result);
    }

    @Test
    public void test_update() {
        //given
        final var timeStamp = Timestamp.valueOf(LocalDateTime.now());

        final var taskDto = TaskDto.builder()
                .id(1L)
                .dateTime(timeStamp)
                .description("UpdatedDescription")
                .name("task")
                .build();

        final var task = new Task()
                .setId(1L)
                .setDateTime(timeStamp)
                .setDescription("UpdatedDescription")
                .setName("task");

        when(taskRepository.save(eq(task))).thenReturn(task);
        //when
        final var result = taskService.update(taskDto, task);

        //then
        assertNotNull(result);
        assertEquals(task, result);
    }

    @Test
    public void test_listAll() {
        //given
        final var userId = 1L;

        final var timeStamp = Timestamp.valueOf(LocalDateTime.now());

        final var task = new Task()
                .setId(1L)
                .setDateTime(timeStamp)
                .setDescription("UpdatedDescription")
                .setName("task");

        final var tasks = Lists.list(task);

        when(taskRepository.findAllByUserId(eq(userId))).thenReturn(tasks);
        //when
        final var result = taskService.listAll(userId);

        //then
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(task, result.get(0));
    }

    @Test
    public void test_delete() {
        //given
        final var timeStamp = Timestamp.valueOf(LocalDateTime.now());

        final var task = new Task()
                .setId(1L)
                .setDateTime(timeStamp)
                .setDescription("UpdatedDescription")
                .setName("task");

        doNothing().when(taskRepository).delete(eq(task));
        //when
        taskService.delete(task);

        //then
        verify(taskRepository, times(1)).delete(eq(task));
    }

    @Test
    public void test_saveAll() {
        //given
        final var timeStamp = Timestamp.valueOf(LocalDateTime.now());

        final var task = new Task()
                .setId(1L)
                .setDateTime(timeStamp)
                .setDescription("UpdatedDescription")
                .setName("task");

        final var tasks = Lists.list(task);

        when(taskRepository.saveAll(eq(tasks))).thenReturn(tasks);
        //when
        taskService.saveAll(tasks);

        //then
        verify(taskRepository, times(1)).saveAll(eq(tasks));
    }

    @Test
    public void test_findDoneTasks() {
        //given
        final var timeStamp = Timestamp.valueOf(LocalDateTime.now());

        final var task = new Task()
                .setId(1L)
                .setDateTime(timeStamp)
                .setDescription("UpdatedDescription")
                .setName("task");

        final var tasks = Lists.list(task);

        when(taskRepository.findAll(any(FindDoneTasksSpecification.class))).thenReturn(tasks);
        //when
        final var result = taskService.findDoneTasks();

        //then
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(task, result.get(0));
    }
}
