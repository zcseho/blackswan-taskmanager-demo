package com.blackswan.taskmanager.demo.service;

import com.blackswan.taskmanager.demo.dao.models.User;
import com.blackswan.taskmanager.demo.dao.repository.UserRepository;
import com.blackswan.taskmanager.demo.domain.service.UserService;
import com.blackswan.taskmanager.demo.domain.service.dto.UserDto;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Test
    public void test_save() {
        //given
        final var userDto = UserDto.builder()
                .userName("userName")
                .firstName("firstName")
                .lastName("lastName")
                .id(1L)
                .build();

        final var user = new User()
                .setUserName("userName")
                .setFirstName("firstName")
                .setLastName("lastName")
                .setId(1L);

        when(userRepository.save(eq(user))).thenReturn(user);

        //when
        final var result = userService.save(userDto);

        //then
        assertNotNull(result);
        assertEquals(user, result);
    }

    @Test
    public void test_get() {
        //given
        final var id = 1L;

        final var user = new User()
                .setUserName("userName")
                .setFirstName("firstName")
                .setLastName("lastName")
                .setId(1L);

        when(userRepository.findById(eq(id))).thenReturn(Optional.ofNullable(user));

        //when
        final var result = userService.get(id);

        //then
        assertNotNull(result);
        assertEquals(user, result);
    }

    @Test
    public void test_get_fail() {
        //given
        final var id = 2L;

        when(userRepository.findById(eq(id))).thenReturn(Optional.empty());

        //when
        //then
        assertThrows(IllegalArgumentException.class, () -> userService.get(id));
    }

    @Test
    public void test_listAll() {
        //given
        final var user = new User()
                .setUserName("userName")
                .setFirstName("firstName")
                .setLastName("lastName")
                .setId(1L);

        final var users = Lists.list(user);

        when(userRepository.findAll()).thenReturn(users);
        //when
        final var result = userService.listAll();

        //then
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(result.get(0), user);
    }

    @Test
    public void test_update() {
        //given
        final var userDto = UserDto.builder()
                .userName("updatedUserName")
                .firstName("firstName")
                .lastName("lastName")
                .id(1L)
                .build();

        final var user = new User()
                .setUserName("userName")
                .setFirstName("firstName")
                .setLastName("lastName")
                .setId(1L);

        final var updatedUser = new User()
                .setUserName("updatedUserName")
                .setFirstName("firstName")
                .setLastName("lastName")
                .setId(1L);

        final var id = 1L;

        when(userRepository.findById(eq(id))).thenReturn(Optional.ofNullable(user));
        when(userRepository.save(updatedUser)).thenReturn(updatedUser);
        //when
        final var result = userService.update(userDto, id);

        //then
        assertNotNull(result);
        assertEquals(updatedUser, result);
    }

    @Test
    public void test_update_fail() {
        //given
        final var userDto = UserDto.builder()
                .userName("updatedUserName")
                .firstName("firstName")
                .lastName("lastName")
                .id(1L)
                .build();

        final var id = 2L;

        when(userRepository.findById(eq(id))).thenReturn(Optional.empty());

        //when
        //then
        assertThrows(IllegalArgumentException.class, () -> userService.update(userDto, id));
    }

}
