package com.blackswan.taskmanager.demo.facade;

import com.blackswan.taskmanager.demo.dao.models.User;
import com.blackswan.taskmanager.demo.domain.facade.UserFacade;
import com.blackswan.taskmanager.demo.domain.service.UserService;
import com.blackswan.taskmanager.demo.domain.service.dto.UserDto;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class UserFacadeTest {

    @InjectMocks
    private UserFacade userFacade;

    @Mock
    private UserService userService;

    @Test
    public void test_save() {
        //given
        final var userDto = UserDto.builder()
                .userName("userName")
                .firstName("firstName")
                .lastName("lastName")
                .id(1L)
                .build();

        final var user = new User()
                .setUserName("userName")
                .setFirstName("firstName")
                .setLastName("lastName")
                .setId(1L);

        when(userService.save(eq(userDto))).thenReturn(user);
        //when
        final var result = userFacade.save(userDto);

        //then
        assertNotNull(result);
        assertEquals(userDto, result);
    }

    @Test
    public void test_get() {
        //given
        final var id = 1L;

        final var userDto = UserDto.builder()
                .userName("userName")
                .firstName("firstName")
                .lastName("lastName")
                .id(1L)
                .build();

        final var user = new User()
                .setUserName("userName")
                .setFirstName("firstName")
                .setLastName("lastName")
                .setId(1L);

        when(userService.get(eq(id))).thenReturn(user);
        //when
        final var result = userFacade.get(id);

        //then
        assertNotNull(result);
        assertEquals(userDto, result);
    }

    @Test
    public void test_listAll() {
        //given
        final var userDto = UserDto.builder()
                .userName("userName")
                .firstName("firstName")
                .lastName("lastName")
                .id(1L)
                .build();

        final var user = new User()
                .setUserName("userName")
                .setFirstName("firstName")
                .setLastName("lastName")
                .setId(1L);

        final var users = Lists.list(user);
        final var userDtoList = Lists.list(userDto);

        when(userService.listAll()).thenReturn(users);
        //when
        final var result = userFacade.listAll();

        //then
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(userDto, result.get(0));
    }
}
