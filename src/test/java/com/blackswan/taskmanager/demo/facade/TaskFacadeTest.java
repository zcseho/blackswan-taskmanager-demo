package com.blackswan.taskmanager.demo.facade;

import com.blackswan.taskmanager.demo.dao.models.Task;
import com.blackswan.taskmanager.demo.dao.models.User;
import com.blackswan.taskmanager.demo.domain.facade.TaskFacade;
import com.blackswan.taskmanager.demo.domain.service.TaskService;
import com.blackswan.taskmanager.demo.domain.service.UserService;
import com.blackswan.taskmanager.demo.domain.service.dto.TaskDto;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class TaskFacadeTest {

    @InjectMocks
    private TaskFacade taskFacade;

    @Mock
    private TaskService taskService;

    @Mock
    private UserService userService;

    @Test
    public void test_save() {
        //given
        final var timestamp = Timestamp.valueOf(LocalDateTime.now());

        final long id = 1L;

        final var user = new User()
                .setId(1L)
                .setUserName("username");

        final var taskDto = TaskDto.builder()
                .id(1L)
                .dateTime(timestamp)
                .description("UpdatedDescription")
                .name("task")
                .build();

        final var task = new Task()
                .setId(1L)
                .setDateTime(timestamp)
                .setDescription("UpdatedDescription")
                .setName("task");

        when(userService.get(eq(id))).thenReturn(user);
        when(taskService.save(taskDto, user)).thenReturn(task);
        //when
        final var result = taskFacade.save(taskDto, id);

        //then
        assertNotNull(result);
        assertEquals(taskDto, result);
    }

    @Test
    public void test_listAll() {
        //given
        final var userId = 1L;

        final var timestamp = Timestamp.valueOf(LocalDateTime.now());

        final var taskDto = TaskDto.builder()
                .id(1L)
                .dateTime(timestamp)
                .description("UpdatedDescription")
                .name("task")
                .build();

        final var task = new Task()
                .setId(1L)
                .setDateTime(timestamp)
                .setDescription("UpdatedDescription")
                .setName("task");

        final var tasks = Lists.list(task);

        when(taskService.listAll(eq(userId))).thenReturn(tasks);
        //when
        final var result = taskFacade.listAll(userId);

        //then
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(taskDto, result.get(0));
    }

    @Test
    public void test_delete() {
        //given
        final var timestamp = Timestamp.valueOf(LocalDateTime.now());

        final var userId = 1L;

        final var taskId = 1L;

        final var task = new Task()
                .setId(1L)
                .setDateTime(timestamp)
                .setDescription("UpdatedDescription")
                .setName("task");

        when(taskService.findByUserIdAndTaskId(eq(userId), eq(taskId))).thenReturn(task);
        doNothing().when(taskService).delete(task);
        //when
        taskFacade.delete(userId, taskId);

        //then
        verify(taskService, times(1)).delete(eq(task));
    }

    @Test
    public void test_get() {
        //given
        final var timestamp = Timestamp.valueOf(LocalDateTime.now());

        final var userId = 1L;

        final var taskId = 1L;

        final var task = new Task()
                .setId(1L)
                .setDateTime(timestamp)
                .setDescription("UpdatedDescription")
                .setName("task");

        final var taskDto = TaskDto.builder()
                .id(1L)
                .dateTime(timestamp)
                .description("UpdatedDescription")
                .name("task")
                .build();

        when(taskService.findByUserIdAndTaskId(eq(userId), eq(taskId))).thenReturn(task);
        //when
        final var result = taskFacade.get(userId, taskId);

        //then
        assertNotNull(result);
        assertEquals(taskDto, result);
    }

}
