package com.blackswan.taskmanager.demo.dto;

import com.blackswan.taskmanager.demo.dao.models.Task;
import com.blackswan.taskmanager.demo.domain.service.dto.TaskDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class TaskDtoTest {

    @Test
    public void test_mappingFromEntity() {
        //given
        final var timeStamp = Timestamp.valueOf(LocalDateTime.now());

        final var taskDto = TaskDto.builder()
                .id(1L)
                .dateTime(timeStamp)
                .description("UpdatedDescription")
                .name("task")
                .build();

        final var task = new Task()
                .setId(1L)
                .setDateTime(timeStamp)
                .setDescription("UpdatedDescription")
                .setName("task");

        //when
        final var result = TaskDto.fromEntity(task);

        //then
        assertNotNull(result);
        assertEquals(taskDto, result);
    }

    @Test
    public void test_mappingToEntity() {
        //given
        final var timeStamp = Timestamp.valueOf(LocalDateTime.now());

        final var taskDto = TaskDto.builder()
                .id(1L)
                .dateTime(timeStamp)
                .description("UpdatedDescription")
                .name("task")
                .build();

        final var task = new Task()
                .setId(1L)
                .setDateTime(timeStamp)
                .setDescription("UpdatedDescription")
                .setName("task");

        //when
        final var result = TaskDto.toEntity(taskDto);

        //then
        assertNotNull(result);
        assertEquals(task, result);
    }

}
