package com.blackswan.taskmanager.demo.dto;

import com.blackswan.taskmanager.demo.dao.models.User;
import com.blackswan.taskmanager.demo.domain.service.dto.UserDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class UserDtoTest {

    @Test
    public void test_mappingFromEntity() {
        //given
        final var userDto = UserDto.builder()
                .userName("userName")
                .firstName("firstName")
                .lastName("lastName")
                .id(1L)
                .build();

        final var user = new User()
                .setUserName("userName")
                .setFirstName("firstName")
                .setLastName("lastName")
                .setId(1L);

        //when
        final var result = UserDto.fromEntity(user);

        //then
        assertNotNull(result);
        assertEquals(userDto, result);
    }

    @Test
    public void test_mappingToEntity() {
        final var userDto = UserDto.builder()
                .userName("userName")
                .firstName("firstName")
                .lastName("lastName")
                .id(1L)
                .build();

        final var user = new User()
                .setUserName("userName")
                .setFirstName("firstName")
                .setLastName("lastName")
                .setId(1L);

        //when
        final var result = UserDto.toEntity(userDto);

        //then
        assertNotNull(result);
        assertEquals(user, result);
    }

}
