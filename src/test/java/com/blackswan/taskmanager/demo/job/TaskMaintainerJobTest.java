package com.blackswan.taskmanager.demo.job;

import com.blackswan.taskmanager.demo.dao.enums.TaskState;
import com.blackswan.taskmanager.demo.dao.models.Task;
import com.blackswan.taskmanager.demo.domain.service.TaskService;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class TaskMaintainerJobTest {

    @InjectMocks
    private TaskMaintainerJob taskMaintainerJob;

    @Mock
    private TaskService taskService;

    @Test
    public void test_maintain() {
        //given
        final var taskDateTime = Timestamp.valueOf(LocalDateTime.now().minusYears(1));

        final var task = new Task()
                .setName("task")
                .setTaskState(TaskState.PENDING)
                .setDateTime(taskDateTime);

        final var updatedTask = new Task()
                .setName("task")
                .setTaskState(TaskState.DONE)
                .setDateTime(taskDateTime);

        final var tasks = Lists.list(task);

        final var updatedTasks = Lists.list(updatedTask);

        when(taskService.findDoneTasks()).thenReturn(tasks);
        doNothing().when(taskService).saveAll(updatedTasks);
        //when
        taskMaintainerJob.maintainTasks();

        //then
        verify(taskService, times(1)).saveAll(eq(updatedTasks));
    }

}
