package com.blackswan.taskmanager.demo.domain.executor;

import com.blackswan.taskmanager.demo.domain.exception.ConcurrentUpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.concurrent.*;

/**
 * custom threadPool executor service initialized with only 1 thread used for concurrent actions
 */
@Component
public class ConcurrentUpdateExecutor<T> extends ThreadPoolExecutor {

    @Autowired
    private TransactionTemplate transactionTemplate;

    public ConcurrentUpdateExecutor() {
        super(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
    }

    /**
     * @param task the task to run wrapped into a transaction session
     * @return the result of the executed process
     */
    @NonNull
    public Future<T> executeTransaction(@NonNull final Callable<T> task) {
        return super.submit(() -> transactionTemplate.execute(action -> {
            try {
                return task.call();
            } catch (final Exception e) {
                throw new ConcurrentUpdateException(e);
            }
        }));
    }

}
