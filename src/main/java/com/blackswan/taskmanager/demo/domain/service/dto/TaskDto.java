package com.blackswan.taskmanager.demo.domain.service.dto;

import com.blackswan.taskmanager.demo.dao.models.Task;
import lombok.Builder;
import org.springframework.lang.NonNull;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Objects;

@Builder
public record TaskDto(
        Long id,
        String name,
        String description,
        Timestamp dateTime
) {

    public static TaskDto fromEntity(@NonNull final Task task) {
        return TaskDto.builder()
                .id(task.getId())
                .name(task.getName())
                .description(task.getDescription())
                .dateTime(task.getDateTime())
                .build();
    }

    public static Task toEntity(@NonNull final TaskDto taskDto) {
        return new Task()
                .setDateTime(Objects.requireNonNullElse(taskDto.dateTime(), Timestamp.valueOf(LocalDateTime.now())))
                .setName(taskDto.name())
                .setDescription(taskDto.description())
                .setId(taskDto.id());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final TaskDto taskDto = (TaskDto) o;
        return Objects.equals(id, taskDto.id)
                && Objects.equals(name, taskDto.name)
                && Objects.equals(description, taskDto.description)
                && Objects.equals(dateTime, taskDto.dateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, dateTime);
    }
}
