package com.blackswan.taskmanager.demo.domain.facade;

import com.blackswan.taskmanager.demo.dao.models.Task;
import com.blackswan.taskmanager.demo.domain.exception.ConcurrentUpdateException;
import com.blackswan.taskmanager.demo.domain.executor.ConcurrentUpdateExecutor;
import com.blackswan.taskmanager.demo.domain.service.TaskService;
import com.blackswan.taskmanager.demo.domain.service.UserService;
import com.blackswan.taskmanager.demo.domain.service.dto.TaskDto;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TaskFacade {

    private final TaskService taskService;
    private final UserService userService;

    /**
     * Thread pool which contains only one thread for concurrent editing
     */
    private final ConcurrentUpdateExecutor<TaskDto> executor;

    /**
     * @param taskDto the incoming task data to be saved
     * @param userId  the id of the user
     * @return the saved task data
     */
    @Transactional
    @NonNull
    public TaskDto save(@NonNull final TaskDto taskDto, @NonNull final Long userId) {
        final var user = userService.get(userId);
        return TaskDto.fromEntity(taskService.save(taskDto, user));
    }

    /**
     * @return all task from the database stored in list
     */
    @Transactional(readOnly = true)
    @NonNull
    public List<TaskDto> listAll(@NonNull final Long userId) {
        return taskService.listAll(userId).stream()
                .map(TaskDto::fromEntity)
                .collect(Collectors.toList());
    }

    /**
     * @param taskDto incoming dataset
     * @param userId  the id of the given user
     * @param taskId  id of the given user's task
     * @return the updated task
     */
    @NonNull
    public TaskDto update(@NonNull final TaskDto taskDto, @NonNull final Long userId, @NonNull final Long taskId) {
        try {
            return executor.executeTransaction(() -> {
                final var task = getUserTaskById(userId, taskId);
                return TaskDto.fromEntity(taskService.update(taskDto, task));
            }).get();
        } catch (Exception e) {
            throw new ConcurrentUpdateException("The update process was not successful !", e);
        }
    }

    /**
     * @param userId the id of the user
     * @param taskId the id of the task to be deleted
     */
    @Transactional
    public void delete(@NonNull final Long userId, @NonNull final Long taskId) {
        final var taskForDelete = getUserTaskById(userId, taskId);
        taskService.delete(taskForDelete);
    }

    /**
     * @param userId the id of the user
     * @param taskId the id of the task to get information about
     * @return the asked task
     */
    @Transactional(readOnly = true)
    @NonNull
    public TaskDto get(@NonNull final Long userId, @NonNull final Long taskId) {
        return TaskDto.fromEntity(getUserTaskById(userId, taskId));
    }

    /**
     * @param userId the id of the user
     * @param taskId the id of the task from a user
     * @return the user's task
     */
    @NonNull
    private Task getUserTaskById(@NonNull final Long userId, @NonNull final Long taskId) {
        return taskService.findByUserIdAndTaskId(userId, taskId);
    }

}
