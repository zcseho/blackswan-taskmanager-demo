package com.blackswan.taskmanager.demo.domain.facade;

import com.blackswan.taskmanager.demo.domain.exception.ConcurrentUpdateException;
import com.blackswan.taskmanager.demo.domain.executor.ConcurrentUpdateExecutor;
import com.blackswan.taskmanager.demo.domain.service.UserService;
import com.blackswan.taskmanager.demo.domain.service.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserFacade {

    private final UserService userService;

    /**
     * Thread pool which contains only one thread for concurrent editing
     */
    private final ConcurrentUpdateExecutor<UserDto> executor;

    /**
     * @param userDto the user data to be saved
     * @return saved user
     */
    @Transactional
    @NonNull
    public UserDto save(@NonNull final UserDto userDto) {
        return UserDto.fromEntity(userService.save(userDto));
    }

    /**
     * @param id the id of the user to get information about
     * @return the asked user
     */
    @Transactional(readOnly = true)
    @NonNull
    public UserDto get(@NonNull final Long id) {
        return UserDto.fromEntity(userService.get(id));
    }

    /**
     * @return all users from the database stored in list
     */
    @Transactional(readOnly = true)
    @NonNull
    public List<UserDto> listAll() {
        return userService.listAll().stream()
                .map(UserDto::fromEntity)
                .collect(Collectors.toList());
    }

    /**
     * @param userDto the updating dataset about the user
     * @param id      the id of the updatable user
     * @return the updated user
     */
    @NonNull
    public UserDto update(@NonNull final UserDto userDto, final Long id) {
        try {
            return executor.executeTransaction(() -> UserDto.fromEntity(userService.update(userDto, id))).get();
        } catch (Exception e) {
            throw new ConcurrentUpdateException("The update process was not successful !", e);
        }
    }

}
