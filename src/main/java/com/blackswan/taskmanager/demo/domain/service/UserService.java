package com.blackswan.taskmanager.demo.domain.service;

import com.blackswan.taskmanager.demo.dao.models.User;
import com.blackswan.taskmanager.demo.dao.repository.UserRepository;
import com.blackswan.taskmanager.demo.domain.service.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    /**
     * @param userDto data of the user to be saved
     * @return the saved user
     */
    @NonNull
    public User save(@NonNull final UserDto userDto) {
        final var user = UserDto.toEntity(userDto);
        return userRepository.save(user);
    }

    /**
     * @param id the id of the user to get information about
     * @return the asked user
     */
    @NonNull
    public User get(@NonNull final Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("The user with the given id does not exists!"));
    }

    /**
     * @return listing all user from the database stored in list
     */
    @NonNull
    public List<User> listAll() {
        return userRepository.findAll();
    }

    /**
     * @param userDto the updating data of the user
     * @param id      the id of the updatable user
     * @return the updated user
     */
    @NonNull
    public User update(@NonNull final UserDto userDto, @NonNull final Long id) {
        final var user = userRepository.findById(id).map(storedUser -> {
                    storedUser.setUserName(Objects.requireNonNullElse(userDto.userName(), storedUser.getUserName()));
                    storedUser.setLastName(Objects.requireNonNullElse(userDto.lastName(), storedUser.getLastName()));
                    storedUser.setFirstName(Objects.requireNonNullElse(userDto.firstName(), storedUser.getFirstName()));
                    return storedUser;
                })
                .orElseThrow(() -> new IllegalArgumentException("The user with the given id does not exists!"));

        return userRepository.save(user);
    }

}
