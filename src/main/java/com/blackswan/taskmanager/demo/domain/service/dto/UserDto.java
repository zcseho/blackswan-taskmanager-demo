package com.blackswan.taskmanager.demo.domain.service.dto;

import com.blackswan.taskmanager.demo.dao.models.User;
import lombok.Builder;
import org.springframework.lang.NonNull;

import java.util.Collections;
import java.util.Objects;

@Builder
public record UserDto(Long id, String userName, String firstName, String lastName) {

    public static UserDto fromEntity(@NonNull final User user) {
        return UserDto.builder()
                .id(user.getId())
                .userName(user.getUserName())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
    }

    public static User toEntity(@NonNull final UserDto userDto) {
        return new User()
                .setFirstName(userDto.firstName())
                .setId(userDto.id())
                .setLastName(userDto.lastName())
                .setTasks(Collections.emptySet())
                .setUserName(userDto.userName());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final UserDto userDto = (UserDto) o;
        return Objects.equals(id, userDto.id)
                && Objects.equals(userName, userDto.userName)
                && Objects.equals(firstName, userDto.firstName)
                && Objects.equals(lastName, userDto.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, firstName, lastName);
    }
}
