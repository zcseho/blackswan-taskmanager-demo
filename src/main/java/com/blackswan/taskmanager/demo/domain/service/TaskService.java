package com.blackswan.taskmanager.demo.domain.service;

import com.blackswan.taskmanager.demo.dao.models.Task;
import com.blackswan.taskmanager.demo.dao.models.User;
import com.blackswan.taskmanager.demo.dao.repository.TaskRepository;
import com.blackswan.taskmanager.demo.dao.specification.FindDoneTasksSpecification;
import com.blackswan.taskmanager.demo.domain.service.dto.TaskDto;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;

    /**
     * @param taskDto task data to be saved
     * @param user    the user who's the task belongs to
     * @return the saved task
     */
    @NonNull
    public Task save(@NonNull final TaskDto taskDto, @NonNull final User user) {
        final var task = TaskDto.toEntity(taskDto);
        task.setUser(user);
        return taskRepository.save(task);
    }

    /**
     * @param taskDto the updating data of the task
     * @param task    the task to be updated
     * @return the updated task
     */
    @NonNull
    public Task update(@NonNull final TaskDto taskDto, @NonNull final Task task) {
        task.setDescription(Objects.requireNonNullElse(taskDto.description(), task.getDescription()));
        task.setName(Objects.requireNonNullElse(taskDto.name(), task.getName()));
        task.setDateTime(Objects.requireNonNullElse(taskDto.dateTime(), task.getDateTime()));
        return taskRepository.save(task);
    }

    /**
     * @return listing all task from the database stored in list
     */
    @NonNull
    public List<Task> listAll(@NonNull final Long userId) {
        return taskRepository.findAllByUserId(userId);
    }

    /**
     * @param task task to be deleted
     */

    public void delete(@NonNull final Task task) {
        taskRepository.delete(task);
    }

    /**
     * @return find PENDING tasks which has expired DATETIME by specification
     */
    @NonNull
    public List<Task> findDoneTasks() {
        return taskRepository.findAll(new FindDoneTasksSpecification());
    }

    /**
     * @param tasks save te tasks contained by the given collection
     */
    public void saveAll(@NonNull final Collection<Task> tasks) {
        taskRepository.saveAll(tasks);
    }

    public Task findByUserIdAndTaskId(@NonNull final Long userId, @NonNull final Long taskId) {
        return taskRepository.findByIdAndUserId(taskId, userId)
                .orElseThrow(() -> new IllegalArgumentException("The given user has no task with the given id!"));
    }

}
