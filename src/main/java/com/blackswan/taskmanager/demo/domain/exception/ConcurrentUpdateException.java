package com.blackswan.taskmanager.demo.domain.exception;

public class ConcurrentUpdateException extends RuntimeException {

    public ConcurrentUpdateException(Throwable cause) {
        super(cause);
    }

    public ConcurrentUpdateException(String message, Throwable cause) {
        super(message, cause);
    }
}
