package com.blackswan.taskmanager.demo.web.resource;

import com.blackswan.taskmanager.demo.domain.service.dto.TaskDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.springframework.lang.NonNull;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Objects;

@Builder
public record TaskResource(
        @JsonProperty("id")
        Long id,

        @NotNull(message = "The name field can not be null!")
        @JsonProperty("name")
        String name,

        @JsonProperty("description")
        String description,

        @JsonProperty("date_time")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
        Timestamp dateTime
) {

    public static TaskDto toTaskDto(@NonNull final TaskResource taskResource) {
        return TaskDto.builder()
                .id(taskResource.id())
                .name(taskResource.name())
                .description(taskResource.description())
                .dateTime(taskResource.dateTime())
                .build();
    }

    public static TaskResource fromDto(@NonNull final TaskDto taskDto) {
        return TaskResource.builder()
                .id(taskDto.id())
                .dateTime(taskDto.dateTime())
                .name(taskDto.name())
                .description(taskDto.description())
                .build();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final TaskResource that = (TaskResource) o;
        return Objects.equals(id, that.id)
                && Objects.equals(name, that.name)
                && Objects.equals(description, that.description)
                && Objects.equals(dateTime, that.dateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, dateTime);
    }
}
