package com.blackswan.taskmanager.demo.web.controller;

import com.blackswan.taskmanager.demo.domain.facade.UserFacade;
import com.blackswan.taskmanager.demo.web.resource.UserResource;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserFacade userFacade;

    @PostMapping
    public UserResource save(@RequestBody final UserResource userResource) {
        return UserResource.fromDto(userFacade.save(UserResource.toUserDto(userResource)));
    }

    @GetMapping("/{id}")
    public UserResource get(@PathVariable final Long id) {
        return UserResource.fromDto(userFacade.get(id));
    }


    @GetMapping
    public List<UserResource> listAll() {
        return userFacade.listAll().stream()
                .map(UserResource::fromDto)
                .collect(Collectors.toList());
    }

    @PutMapping("/{id}")
    public UserResource update(@RequestBody final UserResource userResource, @PathVariable("id") final Long id) {
        return UserResource.fromDto(userFacade.update(UserResource.toUserDto(userResource), id));
    }

}
