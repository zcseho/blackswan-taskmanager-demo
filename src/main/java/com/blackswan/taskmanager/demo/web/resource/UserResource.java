package com.blackswan.taskmanager.demo.web.resource;

import com.blackswan.taskmanager.demo.domain.service.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.springframework.lang.NonNull;

import java.util.Objects;

@Builder
public record UserResource(
        @JsonProperty(value = "id", index = 1)
        Long id,
        @JsonProperty(value = "username", index = 2)
        String userName,
        @JsonProperty(value = "first_name", index = 3)
        String firstName,
        @JsonProperty(value = "last_name", index = 4)
        String lastName) {

    public static UserDto toUserDto(@NonNull final UserResource userResource) {
        return UserDto.builder()
                .id(userResource.id())
                .lastName(userResource.lastName())
                .firstName(userResource.firstName())
                .userName(userResource.userName())
                .build();
    }

    public static UserResource fromDto(@NonNull final UserDto userDto) {
        return UserResource.builder()
                .id(userDto.id())
                .userName(userDto.userName())
                .firstName(userDto.firstName())
                .lastName(userDto.lastName())
                .build();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final UserResource that = (UserResource) o;
        return Objects.equals(id, that.id)
                && Objects.equals(userName, that.userName)
                && Objects.equals(firstName, that.firstName)
                && Objects.equals(lastName, that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, firstName, lastName);
    }
}

