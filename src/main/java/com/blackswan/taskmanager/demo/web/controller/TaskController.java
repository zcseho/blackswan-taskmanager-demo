package com.blackswan.taskmanager.demo.web.controller;

import com.blackswan.taskmanager.demo.domain.facade.TaskFacade;
import com.blackswan.taskmanager.demo.web.resource.TaskResource;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class TaskController {

    private final TaskFacade taskFacade;

    @PostMapping("/{user_id}/task")
    public TaskResource save(@RequestBody final TaskResource taskResource, @PathVariable("user_id") final Long userId) {
        return TaskResource.fromDto(taskFacade.save(TaskResource.toTaskDto(taskResource), userId));
    }

    @GetMapping("/{user_id}/task")
    public List<TaskResource> listAll(@PathVariable("user_id") final Long userId) {
        return taskFacade.listAll(userId).stream()
                .map(TaskResource::fromDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{user_id}/task/{task_id}")
    public TaskResource get(@PathVariable("user_id") final Long userId, @PathVariable("task_id") final Long taskId) {
        return TaskResource.fromDto(taskFacade.get(userId, taskId));
    }

    @PutMapping("/{user_id}/task/{task_id}")
    public TaskResource update(@RequestBody final TaskResource taskResource,
                               @PathVariable("user_id") final Long userId,
                               @PathVariable("task_id") final Long taskId) {
        return TaskResource.fromDto(taskFacade.update(TaskResource.toTaskDto(taskResource), userId, taskId));
    }

    @DeleteMapping("/{user_id}/task/{task_id}")
    public void delete(@PathVariable("user_id") final Long userId, @PathVariable("task_id") final Long taskId) {
        taskFacade.delete(userId, taskId);
    }

}
