package com.blackswan.taskmanager.demo.job;

import com.blackswan.taskmanager.demo.dao.enums.TaskState;
import com.blackswan.taskmanager.demo.domain.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TaskMaintainerJob {

    private final TaskService taskService;

    private final Logger logger = LoggerFactory.getLogger(TaskMaintainerJob.class);

    /**
     * Job to search for all PENDING task which has expired DATETIME to be set to DONE
     */
    @Scheduled(fixedDelay = 1000, initialDelay = 1000)
    @Transactional
    public void maintainTasks() {
        final var tasks = taskService.findDoneTasks().stream()
                .peek(task -> {
                    logger.info("Updating task with name: " + task.getName());
                    task.setTaskState(TaskState.DONE);
                }).collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(tasks)) {
            taskService.saveAll(tasks);
        }
    }

}
