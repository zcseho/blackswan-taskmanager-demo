package com.blackswan.taskmanager.demo.dao.repository;

import com.blackswan.taskmanager.demo.dao.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
