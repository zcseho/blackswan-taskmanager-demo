package com.blackswan.taskmanager.demo.dao.repository;

import com.blackswan.taskmanager.demo.dao.models.Task;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    List<Task> findAll(Specification<Task> specification);

    Optional<Task> findByIdAndUserId(final Long id, final Long userId);

    List<Task> findAllByUserId(final Long userId);

}
