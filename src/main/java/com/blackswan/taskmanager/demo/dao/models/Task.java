package com.blackswan.taskmanager.demo.dao.models;

import com.blackswan.taskmanager.demo.dao.enums.TaskState;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Objects;

@Accessors(chain = true)
@Entity
@Table(name = "tasks")
@Getter
@Setter
public class Task {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "description")
    private String description;

    @Builder.Default
    @Column(name = "date_time")
    private Timestamp dateTime = Timestamp.valueOf(LocalDateTime.now());

    @Builder.Default
    @Enumerated(EnumType.STRING)
    @Column(name = "task_state")
    private TaskState taskState = TaskState.PENDING;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Task task = (Task) o;
        return Objects.equals(id, task.id)
                && Objects.equals(name, task.name)
                && Objects.equals(description, task.description)
                && Objects.equals(dateTime, task.dateTime)
                && taskState == task.taskState;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, dateTime, taskState);
    }
}
