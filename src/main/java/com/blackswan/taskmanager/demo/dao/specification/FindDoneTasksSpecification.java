package com.blackswan.taskmanager.demo.dao.specification;

import com.blackswan.taskmanager.demo.dao.enums.TaskState;
import com.blackswan.taskmanager.demo.dao.models.Task;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.NonNull;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class FindDoneTasksSpecification implements Specification<Task> {

    private static final String TASK_STATE = "taskState";
    private static final String DATE_TIME = "dateTime";

    @Override
    public Predicate toPredicate(final Root<Task> root, @NonNull final CriteriaQuery<?> query, final CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.and(criteriaBuilder.equal(root.get(TASK_STATE), TaskState.PENDING),
                criteriaBuilder.lessThan(root.get(DATE_TIME), Timestamp.valueOf(LocalDateTime.now())));
    }
}
