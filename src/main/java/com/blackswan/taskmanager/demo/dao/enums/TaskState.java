package com.blackswan.taskmanager.demo.dao.enums;

public enum TaskState {
    PENDING, DONE
}
