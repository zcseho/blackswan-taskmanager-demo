create table users(
id bigserial primary key,
user_name text unique,
first_name text,
last_name text
);

create table tasks(
id bigserial primary key,
name text unique,
description text,
date_time timestamp default current_timestamp,
task_state text,
user_id integer,
CONSTRAINT fk_users_id
      FOREIGN KEY(user_id)
	  REFERENCES users(id)
);