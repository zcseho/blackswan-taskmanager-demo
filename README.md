# blackswan-taskmanager-demo

## Description

This is a demo application for black swan. The application is created for managing users and their tasks.

## Usage

For using the application you need to have:

- installed docker
- some kind of IDE

After you cloned the repository you will be able to start the application two ways:

- run the next command in the root folder: **docker-compose up --build**
    - this way the docker compose file will build and run a postgresql database and the application
    - **EASIEST WAY**
- or go to **debugDb** folder and do the next steps:
    - run the next command: **docker-compose up**
        - for processing a postgresql database instance
    - start the project from the **IDE**
        - for this you need to have installed jdk17 and maven 3.8.*

